var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var cookie = require('cookie');
var _ = require('underscore');

var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/game', routes.game);

///////////////////////////////////////////////////////////////////////////////
server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var users = {};
var channel = "test room";
var boss = {
  alive: true,
  x: 300,
  y: 300,
  hp: 100
};

io.sockets.on('connection', function (socket) {
  var uid = cookie.parse(socket.handshake.headers.cookie || "").name

  socket.on('register', function(data) {
    if(!users[uid]) {
      users[uid] = {
        id: uid,
        sid: socket.id,
        x: 0,
        y: 0
      };
    }

    var user = users[uid];
    user.active = true;

    var activeUsers = [];
    _.each(users, function(u, id) {
      if(!u.active) { return; }
      if(id == uid) { return; }
      activeUsers.push(u);
    });

    socket.join(channel);
    socket.emit('connected', {
      channel: channel,
      you: users[uid],
      users: activeUsers,
      boss: boss
    });
    socket.broadcast.to(channel).emit("userJoined", user);
  });

  socket.on('disconnect', function () {
    if(users[uid]) {
      users[uid].active = false;
    }
    socket.broadcast.to(channel).emit("userGone", { id: uid });
  });

  socket.on('move', function (data) {
    if(!users[uid]) { return; }

    users[uid].x = data.x || 0;
    users[uid].y = data.y || 0;
    io.sockets.in(channel).emit("move", { id: uid, x: data.x, y: data.y });
  });

  socket.on('attack', function () {
    io.sockets.in(channel).emit("attack", { id: uid });
  });

  socket.on('damage', function () {
    var damage = _.random(0, 5);
    var crit = (damage > 0 && _.random(0,3) === 3)
    if(crit) {
      damage = 10;
    }

    boss.hp -= damage;
    if(boss.alive) {
      io.sockets.in(channel).emit("damage", {
        id: uid,
        damage: damage,
        crit: crit,
        boss: boss
      });

      if(boss.hp <= 0) {
        boss.alive = false;
        io.sockets.in(channel).emit("bossDefeated", { id: uid });
      }
    }
  });
});

// warp boss to random location every 10 seconds
setInterval(function() {
  if(!boss.alive) {
    return;
  }

  boss.x = _.random(40, 600);
  boss.y = _.random(100, 700);

  io.sockets.in(channel).emit("bossWarp", boss);
}, 5000);
///////////////////////////////////////////////////////////////////////////////
