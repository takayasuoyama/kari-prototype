var OverlayHandler = function() {
  this.scene = core.rootScene;
  this.x = 0;
  this.y = 0;
  this.addTouchHandler();
  this.addAttackHandler();
};

OverlayHandler.prototype = {
  showMouseRipple: function(x, y) {
    var ripple = new Entity();
    ripple._element = $(document.createElement("div"))
      .addClass("my-click")
      .on("webkitAnimationEnd animationend", function() { $(this).remove(); })
      .css({ top: y - 32, left: x - 32 })
      .get(0);
    this.scene.addChild(ripple);
  },

  addAttackHandler: function() {
    var self = this;
    var lastAttackedAt = 0;
    var attackingInterval = 300;

    var button = $(document.createElement("div"))
      .text("攻撃")
      .addClass("btn btn-lg btn-warning")
      .css({ position: 'fixed', bottom: 0, left: 20, width: 200, height: 100 })
      .on("touchstart mousedown",function() {
        var delta = new Date().getTime() - lastAttackedAt;
        if(delta > attackingInterval) {
          lastAttackedAt = new Date().getTime();
          socket.emit("attack");
        }
        return false;
      });
    $(document.body).append(button);    
  },

  addTouchHandler: function() {
    var self = this;
    var isMouseDown = false;
    var lastUpdate = null;
    var mouseMoveRegisterInterval = 100;

    this.scene.on("touchstart", function(e) {
      socket.emit("move", { x: e.x, y: e.y });
      lastUpdate = new Date().getTime();
      isMouseDown = true;
      self.x = e.x;
      self.y = e.y;
      self.showMouseRipple(e.x, e.y);
    });

    this.scene.on("touchend", function(e) {
      isMouseDown = false;
      self.x = e.x;
      self.y = e.y;
    });

    this.scene.on("touchmove", function(e) {
      if(!isMouseDown) { return; }
      self.x = e.x;
      self.y = e.y;

      var delta = new Date().getTime() - lastUpdate;
      if(delta > mouseMoveRegisterInterval) {
        socket.emit("move", { x: e.x, y: e.y });
        lastUpdate = new Date().getTime();
        self.showMouseRipple(e.x, e.y);
      }
    });

    return this;
  }
};
