var Unit = enchant.Class.create(enchant.Group, {
  initialize: function(userData) {
    enchant.Group.call(this);
    this.id = userData.id;
    this.lastSync = 0;
    this.radian = 0;

    this.x = userData.x || 0;
    this.y = userData.y || 0;
    this.toX = userData.x || 0;
    this.toY = userData.y || 0;

    this.tl.setTimeBased();
    this.sprite = new UnitSprite(32, 32);
    this.sprite.x = -this.sprite.width / 2;
    this.sprite.y = -this.sprite.height/ 2;
    this.addChild(this.sprite);

    this.director = new Entity();
    this.director.width = 2;
    this.director.height = 2;
    this.director.backgroundColor = "blue";
    this.addChild(this.director);

    var label = new Label(userData.id);
    label.font = '11px sans-serif';
    label.moveTo(label._boundWidth / -2, -25);
    this.addChild(label);
  },

  action: function(boss) {
    var dx = this.toX - this.x;
    var dy = this.toY - this.y;
    var pps = 4.0;

    var radian = Math.atan2(dy, dx);

    var prevX = this.x;
    var prevY = this.y;

    if(Math.abs(dx) >= pps || Math.abs(dy) >= pps) {
      this.radian = radian;

      var px = Math.cos(radian);
      var py = Math.sin(radian);

      var x = this.x + pps * px;
      var y = this.y + pps * py;

      this.moveTo(x, y);
      this.director.moveTo(25 * px, 25 * py);
    } else {
      this.moveTo(this.toX, this.toY);
    }
    if(boss.sprite.within(this.sprite)) {
      this.moveTo(prevX, prevY);
    }
  },

  attack: function(boss) {
    var px = Math.cos(this.radian);
    var py = Math.sin(this.radian);

    var effect = new UnitAttackEffect();
    effect.moveTo(25 * px - effect.width/2, 25 * py - effect.height/2); 
    effect.scale(2,2);
    this.addChild(effect);
  },

  update: function(x, y) {
    this.toX = x;
    this.toY = y;
  },

  congrats: function() {    
    var label = new Label("☆ぉめでと☆");
    label.font = '40px sans-serif';
    label.color = 'blue';
    label.moveTo(label._boundWidth / -2, -25);
    this.addChild(label);

    label.tl.setTimeBased();
    label.tl.moveTo(label._boundWidth / -2, -80, 500).fadeOut(10000);
  },

  remove: function() {
    this.scene.removeChild(this);
  }
});

var UnitAttackEffect = enchant.Class.create(enchant.Sprite, {
  initialize: function() {
    var self = this;
    enchant.Sprite.call(this, 16, 16);
    this.image = core.assets['images/unit/effect0.png'];

    var frames = [0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,4,4,4,4,4];
    var endAge = frames.length - 1;

    this.frame = frames;
    this.on(Event.ENTER_FRAME, function() {
      if(this.age > endAge) {
        self.remove();
      }
    });
  },

  remove: function() {
    this.parentNode.removeChild(this);
  }
});

var UnitSprite = enchant.Class.create(enchant.Sprite, {
  initialize: function(width, height) {
    enchant.Sprite.call(this, width, height);

    var self = this;

    this.image = core.assets['images/unit/chara1.png'];
    this.animate = {
      idle: function() { self.frame = [6]; },
      walk: function() { self.frame = [6, 6, 6, 6, 7, 7, 7, 7]; }
    }

    this.animate.walk();
  }
});