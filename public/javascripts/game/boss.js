var Boss = enchant.Class.create(enchant.Group, {
  initialize: function(bossData) {
    enchant.Group.call(this);
    this.id = bossData.id;
    this.x = bossData.x || 0;
    this.y = bossData.y || 0;

    this.tl.setTimeBased();
    this.sprite = new BossSprite(83, 105);
    this.sprite.x = -40;
    this.sprite.y = -40;
    this.addChild(this.sprite);
  },

  damage: function(bossData) {
    var label = new Label();
    label.text = bossData.damage || "(´・ω・`)";
    label.font = '24px sans-serif';
    label.color = 'black';

    if(bossData.crit) {
      label.text = bossData.damage + '！';
      label.font = '30px sans-serif';
      label.color = 'red';
    }
    this.addChild(label);

    label.tl.setTimeBased();
    label.tl.moveBy(0, -50, 1000).then(function() {
      label.parentNode.removeChild(label);
    });
  },

  defeated: function() {
    this.sprite.tl.setTimeBased();
    this.sprite.tl.fadeOut(1000);
  },

  remove: function() {
    this.scene.removeChild(this);
  }
});

var BossSprite = enchant.Class.create(enchant.Sprite, {
  initialize: function(width, height) {
    enchant.Sprite.call(this, width, height);

    var self = this;

    this.image = core.assets['images/unit/shelter.png'];
    this.animate = {
      idle: function() { self.frame = [5]; },
      walk: function() {
        var frame = [];
        _(5).times(function(n) { frame.push(0); });
        _(5).times(function(n) { frame.push(1); });
        _(5).times(function(n) { frame.push(2); });
        _(5).times(function(n) { frame.push(3); });
        self.frame = frame;
      },
    }

    this.animate.walk();
  }
});