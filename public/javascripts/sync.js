var sync = (function() {
  var socket = null;
  self.id = null;

  self.start = function() {
    socket = io.connect('http://192.168.1.138');

    socket.on('connected', function (data) {
      console.log(data);
      toastr.info('Connected to ' + data.channel + ' as ' + data.id);
    });
  }

})();