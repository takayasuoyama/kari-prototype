var socket = io.connect(location.hostname + ":" + location.port);
toastr.options.showMethod = 'slideDown';
toastr.options.hideMethod = 'slideUp';
toastr.options.newestOnTop = false;
toastr.options.positionClass = 'toast-top-full-width';

enchant();

core = new Core(640, 800);
core.fps = 30;
core.scale = 1.0;
core.preload(
  'images/unit/chara1.png',
  'images/unit/effect0.png',
  'images/unit/shelter.png',
  'images/unit/monster/bigmonster1.gif'
);

core.onload = function() {
  core.rootScene.backgroundColor = '#71C671';

  var boss = null;
  var myUnit = null;
  var units = {};

  socket.on('connected', function (data) {
    console.log(data);
    toastr.info('Connected to ' + data.channel + ' as ' + data.you.id);

    myUnit = new Unit(data.you);
    units[data.you.id] = myUnit;
    core.rootScene.addChild(myUnit);

    boss = new Boss(data.boss);
    core.rootScene.addChild(boss);

    _.each(data.users, function(unit) {
      console.log("Remote User:" + unit.id + " -> " + unit.x + ", " + unit.y);
      units[unit.id] = new Unit(unit);
      core.rootScene.addChild(units[unit.id]);
    });

    new OverlayHandler();
  });

  socket.on('userJoined', function (data) {
    console.log("User Joined: " + data.id + " -> " + data.x + ", " + data.y);
    units[data.id] = new Unit(data);
    core.rootScene.addChild(units[data.id]);
  });

  socket.on('userGone', function (data) {
    units[data.id].remove();
  });

  socket.on('move', function (data) {
    units[data.id].update(data.x, data.y);
  });

  socket.on('attack', function (data) {
    units[data.id].attack(boss);

    if(data.id === myUnit.id && myUnit.director.intersect(boss.sprite)) {
      socket.emit('damage');
    }
  });

  socket.on('damage', function(data) {
    boss.damage(data);
  });

  socket.on('bossWarp', function(data) {
    boss.moveTo(data.x, data.y);
  });

  socket.on('bossDefeated', function(data) {
    units[data.id].congrats();
    boss.defeated();
  });

  core.rootScene.on("enterframe", function(e) {
    _.each(units, function(unit) {
      unit.action(boss);
    });
  });
};
core.start();
socket.emit('register');
